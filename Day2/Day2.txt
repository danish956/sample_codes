//MNIT_IIITK_FAST_TRACK_ANDROID_DAY_2    
1. Explain the Folder Structure of the SDK 
	> Platforms Contains all the SDK
	> Platforms-tools & tools contains all the necessary tools to build an android app.
	> Sources has the source code.
	> System-images contains all the images for emulators.
	> Docs has all the documentation for Android app development.
	
1. SDK Manager

1. AVD Manager
	
1. android list target

1. android list avd
	
1. emulator -avd <avd_name>
	
1. android create avd -n myAVD -t 20 -c 1024M

1. Show Android device monitor (DDMS, Dalvik Debugging Monitoring System) 
	Explore the File manager 

1. Push and Pull file from SD card

1. adb devices (with one emulator)

1. adb shell

1. adb devices(with emulator and actual phone)

1. adb -s <device_name> shell

1. Show /data/app in both emulator and device to justify diffrence (rooted and non-rooted devices )

1. adb -s <device_name> pull data/app/ApiDemos/ApiDemos.apk (to pull any file from emulator)

1. adb -s <device_name> install test.apk

1. adb shell 
ls................. show directory (alphabetical order)
mkdir.............. make a directory
rmdir.............. remove directory 
rm-r .............. to delete folders with files
rm................. remove files
mv................. moving and renaming files
cat ................ displaying short files
cd................. change current directory
pwd................ find out what directory you are in
df................. shows available disk space
chmod.............. changes permissions on a file
date ............... display date
exit ............... terminate session
	
1. telnet localhost 5554 through DDMS
	sms send <Senders phone number> <text message>
	
	gsm call 9928492120
	
	Ctrl + ]
	
	quit

1. Screen shot from android device monitor through DDMS

Extra Reading
1. http://codetheory.in/android-debug-bridge-adb-wireless-debugging-over-wi-fi/

##################################
Quick tour of the Android Studio
##################################

1. App->res->Layout->activity_main.xml and double click
	then change the mode from Design to Text
	
1. Fix the Rendering Problem and show the steps, fix build path

1. Similarly show App->java-> and double click a java file

1. Show Ctrl+z to undo and Ctrl+Shift+z to redo 

1. Show different ways to view the file and folder
	On the left select Project, then dropdown
	Android View and Project View
	Project view is the way it is stored in the filesystem
	Android view is the best way
	
1. Use a file explorer and show the same files on the filesystem

1. Explain the .xml file with the xmlns attribute and specific 
	use of android keyword

1. click on the Button in the xml to show the respective widgets on the preview with view border

1. You should always set a different colour to the view border and view background
 to understand the concept of layout
	
1. Explain the Preview button on the right side

1. Explain the top menus for the Preview Window

1. Click and drag a button from the Design and 
	show the Component Tree and Properties Windows

1. Right click on any properties and Show documentation 

1. Show the Android Monitor and Messages from the bottom

1. Show the options for getting help 
( F2, CTRL+Click, API Documentation). 

1. Websites for references 
	http://developer.android.com
	http://www.androidhive.info/
	http://vogella.com/
	http://stackoverflow.com/

1. Copy and Paste the errors in Google with adding keyword 
like Java or Android Studio or Android


1. Give  the definition of the Views in Android
	Its a rectangle on the screen which shows some content ( Image, Text or Button )
	Collection of these individual views make up the layout of the screen
	Whatever you see and interact is know an UI ( User Interface)
	What ever you experience is known as UX ( User Experience )

1. Show them by colouring the background and border with different colors

1. Give students some homescreen of popular apps and let them visualise the 
	types of views in it.
	Charity Miles
	Inbox by Google
	
1. Show the TextView XML and the UI for the same and let students observe and note
	weird angle brackets
	android:text
	
1. Explain the XML syntax 
	opening angle bracket ( < )
	View Name in camel case for the Element ( TextView )
	List of attributes ( android:text="" )
	Self closing angle bracket ( /> ) if no children are there
	or
	opening tag ( <TextView attribute1  attribute2 attribute3  > )
	closing tag  ( </TextView> )
	to explain the concept of Parent and Child tags with example of LinearLayout
	Attribute Name and values in Quotations
	Concept of default values to the attributes
	
1. 	Use the XML Editor for HelloWorld program and let students do hands on and 
	see the changes in the UI for TextView
	
	android:text="Happy Birthday"
	android:background="@android:color/darker_gray"
	android:layout_width="150dp"
	android:layout_height="75dp"

	android:textColor="@android:color/white"
	android:textSize="45sp"
	android:textAppearance="?android:textApperanceLarge"
	
	Change the sequence of the attributes
	Remove some quotation and see the error
	Remove the closing angle bracket and see the error
	Misspell the TextView element and see the error
	Misspell the attribute and see the error
	Write some attribute out size the angle brackets
	
	convert the self closing TextView to opening and closing tags
	
	Increase the text and check the wrapping of the text in the box 
	with more than one line	
	To set it, we need to increase the width
	
1.	Introduce the solution by wrap_content for width and height
	Change the color to blue and then show the concept of using hex code
		
	Read the error message
	Compare to the Common Android Cheat Sheet or working code
	Google the error message
	Ask for help from your peers and then Forsk Team
		
	
1.	Refer to the TextView on the google and goto developer android website 
	Change the text to ALL CAPS and also make it BOLD or ITALIC, use ctrl+F
 	
1. Explain the concept of Density Independent Pixel (dp)
	using three devices of the same physical size but different resolutions

1. Specification for Nexus 5	
	http://gadgets.ndtv.com/lg-google-nexus-5-1115
	
	(height x width x thickness) 137.84 x 69.17 x 8.59 
	Screen size (inches) 	4.95 
	Resolution 	1080x1920 pixels 
	Pixels per inch (PPI) 	445 
		
1. How big is the 2013 Nexus 7's screen in density independent pixel ?
	960x600
	
1. Tip 
	Design and spec layout in dp units
	Create PNG graphics assets for each density to avoid automatic scaling
	
1. Explain the concept of Scale Independent Pixel (sp)

	https://www.google.com/design/spec/material-design/introduction.html
	https://www.google.com/design/spec/style/typography.html
	https://www.google.com/design/spec/style/color.html
	
1. 	Use the XML Editor for HelloWorld program and 
	let students do hands on and see the changes in the UI for ImageView
	
	<ImageView
	android:src="@drawable/cake"
	android:layout_width="100dp"
	android:layout_height="100dp"
	android:scaleType="center"
	/>

	How to add images in the drawable folder
	
	Lowercase concept of files name in drawable folder
	
	no use of extension to refer the files
	
1.  Full bleed images is without the white at the borders
	Convert the width and height to wrap_content
	Apply the scaleType to center and then centerCrop
	change the image to a smaller image and 
	then try the width/height/scaleType
