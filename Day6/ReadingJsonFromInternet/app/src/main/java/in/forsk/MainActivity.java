package in.forsk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.forsk.wrapper.FacultyWrapper;

public class MainActivity extends Activity {

    private final static String TAG = MainActivity.class.getSimpleName();
    private Context context;

    TextView tv;
    Button btnDownload;

    String raw_json = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        tv = (TextView) findViewById(R.id.textView1);
        btnDownload = (Button) findViewById(R.id.button1);

        btnDownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Write the code to read the json file from raw folder.
//                     raw_json = Utils.getStringFromRaw(context, R.raw.faculty_profile_code);
                //Step 1
//                 raw_json = readJsonFromInternet("http://demo5392516.mockable.io/faculty_json");

                //step 2
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        raw_json = readJsonFromInternet("http://demo5392516.mockable.io/faculty_json");
//
//                        showFormattedJson(raw_json);
//                    }
//                }).start();

                //Step 3
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        raw_json = readJsonFromInternet("http://demo5392516.mockable.io/faculty_json");
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                showFormattedJson(raw_json);
//                            }
//                        });
//                    }
//                }).start();

                //Step 4
                new CustomAsyncTask().execute("http://demo5392516.mockable.io/faculty_json");


            }
        });
    }

    // Params, Progress, Result
    class CustomAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = readJsonFromInternet("http://demo5392516.mockable.io/faculty_json");
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Toast.makeText(context, "onPostExecute", Toast.LENGTH_SHORT).show();

            showFormattedJson(result);
        }

    }

    public String readJsonFromInternet(String url) {
        String response = "";
        try {
            InputStream is = Utils.openHttpConnection(url);
            response = Utils.convertStreamToString(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }


    public void showFormattedJson(String raw_json) {

        ArrayList<FacultyWrapper> facultyWrapperArrayList = Utils.pasreLocalFacultyJson(raw_json);

        StringBuilder sb = new StringBuilder();

        for (FacultyWrapper facultyWrapper : facultyWrapperArrayList) {

            sb.append("#############################");
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getFirst_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getLast_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getPhoto());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getDepartment());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getReserch_area());
            sb.append(System.getProperty("line.separator"));

            ArrayList<String> interest_areas = facultyWrapper.getInterest_areas();

            for (String s : interest_areas) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }

            ArrayList<String> contact_details = facultyWrapper.getContact_details();

            for (String s : contact_details) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }
        }

        sb.append("#############################");

        tv.setText(sb);

    }


}
